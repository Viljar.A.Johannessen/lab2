package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    //field variables for items in the fridge and its capacity
    int max_size = 20;
    ArrayList<FridgeItem> fridgeStock;

    //constructor for adding items to fridge
    public Fridge(){
        fridgeStock = new ArrayList<>();
    }

    //methods
    @Override
    public int nItemsInFridge(){
        return fridgeStock.size();
    }

    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < max_size) {
            return fridgeStock.add(item);
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeStock.contains(item)){
            fridgeStock.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeStock.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expiredFridgeItems = new ArrayList<>();
        
        for (FridgeItem i: fridgeStock){
            if (i.hasExpired()){;
            expiredFridgeItems.add(i);
            }
        }
        fridgeStock.removeAll(expiredFridgeItems);
        return expiredFridgeItems;
    }
}